<?php

namespace app\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use app\models\Personajes;
use app\models\Jugadores;
use yii\filters\AccessControl;
use yii\data\SqlDataProvider;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }
    /* consulta 1 */
    public function actionConsulta1a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Personajes::find() ->select("COUNT(*) cod_personaje")->distinct(),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['cod_personaje'],
            "titulo"=>"¿Cuántos personajes tiene el DOOM?",
            "enunciado"=>"Prersonajes que tiene el juego",
            "sql"=>"Solo con esta opcion veras el número de personajes totales.",
            
        ]);
    }
 /* consulta2 */
    public function actionConsulta2a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Razas::find() ->select("COUNT(*) nombre_raza")->distinct(),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre_raza'],
            "titulo"=>"¿Cuántos personajes tiene el DOOM?",
            "enunciado"=>"Prersonajes que tiene el juego",
            "sql"=>"Solo con esta opcion veras el número de Razas de demonios  que tiene el juego.",
            
        ]);
    }
    
     /* consulta3 */
    public function actionConsulta3a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Armas::find() ->select("municion")->distinct()->where("nombre='escopeta'"),
            'pagination'=>[
                    'pageSize'=>0,
                ]
            ]);
        
        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['municion'],
            "titulo"=>"¿Además de poder llevar cartuchos la escopeta, puede tener algo más?",
            "enunciado"=>"Municiones extra de la escopeta",
            "sql"=>"Solo con esta opción, veras el otro tipo de munición que tiene la escopeta en el juego.",
            
        ]);
    }
    /* consulta4 */
    public function actionConsulta4a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => Jugadores::find() ->select("nombre,cod_personaje")->distinct()->where("cod_personaje > 6000"),
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre','cod_personaje'],
            "titulo"=>"¿Qué jugadores tuvieron la mayor puntuación en modo arcade?",
            "enunciado"=>"Jugadores con mayor puntuación",
            "sql"=>"aqui solo podras ver los jugadores con mayor puntuacion en arcade acomañados de la puntuacion que tienen, siendo solo los que mas alta la tengan.",
            
        ]);
    }
    
/* consulta5 */
    public function actionConsulta5a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Demonios::find() ->select("numero, nombre_razas, nombre_mapas")->distinct()->where("numero > 2 or numero < 10"),
            
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['numero','nombre_razas', 'nombre_mapas'],
            "titulo"=>"¿Cuál es la cantidad de demonios y en que mapas puedo verlos?",
            "enunciado"=>"cantidad y demonios dónde suelen aparecer el juego",
            "sql"=>"Aquí sólo se verá la cantidad de demonios, con su nombre y lugar donde suelen estar.",
            
        ]);
    }
    
    /* consulta6 */
    public function actionConsulta6a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Jugadores::find() ->select(" nombre, cod_jugador, sum(cod_personaje)")->distinct()->where("cod_personaje > 3000"),
            
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['cod_jugador','nombre'],
            "titulo"=>"¿Cuál es la media del jugador con mayor puntuacion segun sus partidas en España?",
            "enunciado"=>"Nombre del jugador con mejores estadisticas y posición en ranking mundial",
            "sql"=>"Aquí sólo se verá al jugadro con mejor estadisticas y su posición en el ranking mundial.",
            
        ]);
    }
    
    
       /* consulta7 */
    public function actionConsulta7a(){
      


 $dataProvider = new SqlDataProvider([

'sql'=>"SELECT DISTINCT d.nombre_demonio, h.nombre_raza
    FROM demonios d
    JOIN habilidades h 
    ON d.nombre_razas = h.nombre_raza",


'pagination'=>[
'pageSize' => 5,
]
]);


        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['nombre_raza','nombre_demonio'],
            "titulo"=>"¿Cuál es el nombre de los demonios y cual es su raza?",
            "enunciado"=>"Nombre de los demonios con la raza a la que pertenecen",
            "sql"=>"Aquí sólo se verás el nombre del demonio y a la raza la cual pertenece.",
            
        ]);
    }
 /* consulta8 */
    public function actionConsulta8a(){
        //Mediante active record
        $dataProvider = new ActiveDataProvider([
            'query' => \app\models\Zonas::find() ->select(" count(*) cod_zona")->distinct(),
            
            'pagination'=>[
                    'pageSize'=>5,
                ]
            ]);

        return $this->render("resultados",[
            "resultado"=>$dataProvider,
            "campos"=>['cod_zona'],
            "titulo"=>"¿Cuantas zonas secretas tiene el juego en los mapas en total?",
            "enunciado"=>"Número de zonas secretas en total que tiene el juego todos los mapas juntos",
            "sql"=>"Aquí sólo se verá el número total de zonas secretas de todos los mapas.",
            
        ]);
    }
    
    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';
        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}

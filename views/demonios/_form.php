<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Demonios */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="demonios-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_demonio')->textInput() ?>

    <?= $form->field($model, 'cod_personaje')->textInput() ?>

    <?= $form->field($model, 'numero')->textInput() ?>

    <?= $form->field($model, 'nombre_razas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_mapas')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'nombre_demonio')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

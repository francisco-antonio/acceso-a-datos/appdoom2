<?php

/* @var $this yii\web\View */

use yii\helpers\Html;

$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
?>
<!-- Galeria -->
	<section class="gallery py-5" id="gallery">
		<div class="container py-lg-5 py-3">
                    
                    
                    <!-- logo de la galeria DOOM WIKI APP y imagenes de la galeria -->
                    
                   
                    
			<p class="text-center"> <?= Html::img('@web/images/logo galeria.png', ['alt'=>'Logotipo Galeria doom'], ['class'=>'img-fluid'] );?></p>
			<!--<h3 class="heading mb-4 text-center">Galeria</h3>-->
			<div class="row news-grids text-center">
                            
                            
                            <!-- imagenes a cambiar -->
				<div class="col-md-4 col-sm-6 gal-img">
                                      
					<a href="#gal1"><?= Html::img('@web/images/merodeador.jpg', ['alt'=>'foto1 galeria'], ['class'=>'img-fluid'] );?></a>
                                        <a href="#gal2"><?= Html::img('@web/images/prota 1.jpg', ['alt'=>'foto2 galeria'], ['class'=>'img-fluid'] );?></a>
				

				</div>

				<div class="col-md-4 col-sm-6 gal-img">
					<a href="#gal3"><?= Html::img('@web/images/baron del infierno.jpg', ['alt'=>'imagen galeria 3.1'], ['class'=>'img-fluid']);?></a>
                                   
					<a href="#gal4"><?= Html::img('@web/images/cyberdemonio.jpg', ['alt'=>'imagen galeria 4.1'], ['class'=>'img-fluid']);?></a>
				</div>

				<div class="col-md-4 col-sm-6 gal-img">
                                   
					<a href="#gal5"> <?= Html::img('@web/images/Mancubus-Eternal.png', ['alt'=>'imagen galeria 5.1'], ['class'=>'img-fluid']);?></a>
					<a href="#gal6"><?= Html::img('@web/images/doom-eternal-glory-kill.png', ['alt'=>'imagen galeria 6.1'], ['class'=>'img-fluid']);?></a>
				</div>
                            
                            <!-- nuevas imagenes añadidas 1-->
                            <div class="col-md-4 col-sm-6 gal-img">
                                      
					<a href="#gal11"><?= Html::img('@web/images/apariciones.png', ['alt'=>'foto1 galeria'], ['class'=>'img-fluid'] );?></a>
                                        <a href="#gal12"><?= Html::img('@web/images/D4_Centinelas.jpg', ['alt'=>'foto2 galeria'], ['class'=>'img-fluid'] );?></a>
				

				</div>
                            
                              <!-- nuevas imagenes añadidas 2 -->

                            <div class="col-md-4 col-sm-6 gal-img">
					<a href="#gal7"><?= Html::img('@web/images/DE_Valen.png', ['alt'=>'imagen galeria 3.1'], ['class'=>'img-fluid']);?></a>
                                   
					<a href="#gal8"><?= Html::img('@web/images/DE_Samur_Maykr.png', ['alt'=>'imagen galeria 4.1'], ['class'=>'img-fluid']);?></a>
				</div>
                              
                                  <!-- nuevas imagenes añadidas 3 -->
                               <div class="col-md-4 col-sm-6 gal-img">
					<a href="#gal9"><?= Html::img('@web/images/samel hayden.jpg', ['alt'=>'imagen galeria 3.1'], ['class'=>'img-fluid']);?></a>
                                   
					<a href="#gal10"><?= Html::img('@web/images/khan maykr.png', ['alt'=>'imagen galeria 4.1'], ['class'=>'img-fluid']);?></a>
				</div>
                                  
                                  
			</div>
		</div>
		<!-- popup MIRAR ARREGLAR Y CAMBIAR DESCRIPCION-->
		<div id="gal1" class="pop-overlay animate">
			<div class="popup">
                            <?= Html::img('@web/images/merodeador.jpg', ['alt'=>'imagen galeria 1.1'], ['class'=>'img-fluid']);?>
				
				<p class="mt-4">El Acechador (Marauder) es un nuevo tipo de monstruo, 
                                que aparece como el segundo jefe que debe enfrentar el Doom Slayer en Doom Eternal.
                                Durante la Guerra Civil en Sentinel Prime, varios Centinelas de la noche desilusionados desertaron al bando de Khan Maykr, 
                                poniéndose en contra de los Centinelas Leales. Después de que algunos de ellos cayeron en la batalla, 
                                fueron resucitados y aumentados con la fusión de la tecnología Hell y 
                                Maykr para servir como soldados de choque para las legiones infernales. 
                                Ahora están a la caza para destruir al Slayer.</p>
                                
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->

		<!-- popup arreglar imagenes-->
		<div id="gal2" class="pop-overlay animate">
			<div class="popup">
                            
                            <?= Html::img('@web/images/prota 1.jpg', ['alt'=>'imagen galeria 2.1'], ['class'=>'img-fluid']);?>
				
				<p class="mt-4">The Doom Slayer es el protagonista y personaje jugable de Doom, Doom II: Hell on Earth, 
                                    Final Doom, Doom 64 , Doom (2016) , Quake 3 Arena, Quake Champions y Doom Eternal . 
                                    También es conocido por el UAC como DM1-5 , DOOM Marine o The Slayer , 
                                    y por los Demonios del Infierno como Doom Slayer , Unchained Predator , 
                                    The Beast o Hell Walker . Dependiendo de la versión del personaje, 
                                    su nombre real o alias incluyenWilliam Joseph Blazkowicz y Flynn Taggart . 
                                    Es conocido en los subtítulos de flashback como Doomguy .</p>
                                
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
                
		<!-- popup numero 3-->
		<div id="gal3" class="pop-overlay animate">
			<div class="popup">
                            <?= Html::img('@web/images/baron del infierno.jpg', ['alt'=>'imagen dentro POPUP 3.1'], ['class'=>'img-fluid']);?>
	
				<p class="mt-4">Los Barones del Infierno son monstruosos enemigos que parecen minotauros, 
                                    faunos o sátiros con los torsos de color rosa, cuernos y patas de cabra marrón.
Un par de Barones, denominado internamente por id Software como "Bruiser Brothers," 
aparecen como los jefes al final de Knee-Deep in the Dead, el primer episodio de Doom.
Los Barones también aparecen como enemigos regulares en los episodios posteriores y en las secuelas; coincidentemente, 
ellos y sus contrapartes más débiles los Caballeros del infierno de Doom II aparecen con frecuencia en parejas.</p>
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup3 -->
		<!-- popup-->
		<div id="gal4" class="pop-overlay animate">
			<div class="popup">
                             <?= Html::img('@web/images/cyberdemonio.jpg', ['alt'=>'imagen dentro POPUP 4.1'], ['class'=>'img-fluid']);?>
			
				<p class="mt-4">The Tyrant es una recreación del Cyberdemon original de Doom en lugar de la variante más voluminosa y 
                                    pesada que se encontró en el juego anterior. 
                                    Es de color tostado, con largos cuernos negros curvados y dos pequeños ojos negros y brillantes,
                                    muy separados entre sí. El tirano tiene una cara plana con fosas nasales al ras y carece de cualquier tipo de labios, 
                                    exponiendo dientes afilados con dos grandes colmillos hacia arriba como los caninos inferiores. 
                                    Carece de la mandíbula dividida que se encuentra en la versión Doom 2016 del monstruo.</p>
                                
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
		<!-- popup-->
		<div id="gal5" class="pop-overlay animate">
			<div class="popup">
                             <?= Html::img('@web/images/Mancubus-Eternal.png', ['alt'=>'imagen dentro POPUP 5.1'], ['class'=>'img-fluid']);?>
			
				<p class="mt-4">El Mancubus, gigantesco gigante del mundo de los demonios, 
                                    tiene notables diferencias con las de su clase que  el equipo de expedición UAC encontró por primera vez en Marte. 
                                    A diferencia de sus hermanos que usaban un caparazón blindado natural, 
                                    esta variación muestra signos claros de modificaciones preexistentes hechas por el hombre. 
                                    El aumento de blindaje en su exterior y un sofisticado sistema de arma montado en el brazo indican una clara intención de 
                                    mejorar y convertir en arma la capacidad de combate del Mancubus, 
                                    cuya gran estatura y resistencia lo convierten en una amenaza formidable en la batalla.</p>
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
		<!-- popup-->
		<div id="gal6" class="pop-overlay animate">
			<div class="popup">
                             <?= Html::img('@web/images/doom-eternal-glory-kill.png', ['alt'=>'imagen dentro POPUP 6.1'], ['class'=>'img-fluid']);?>
				
				<p class="mt-4">Los Soldados (también llamados Soldados Hellificados) regresan como enemigos en Doom Eternal, tomando el lugar del Soldado poseído de Doom (2016).

Son antiguos seres humanos poseídos por un poder demoníaco desconocido, 
que se vuelven contra su propia raza para luchar junto a los invasores demoníacos. 
Si bien cumplen el mismo papel que los soldados poseídos y los Guardias Poseídos, 
han sido rediseñados para parecerse más a su aspecto en el Doom original.</p>
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
                
                <!-- popup añadido-->
		<div id="gal7" class="pop-overlay animate">
			<div class="popup">
                             <?= Html::img('@web/images/DE_Valen.png', ['alt'=>'imagen dentro POPUP 4.1'], ['class'=>'img-fluid']);?>
			
				<p class="mt-4">Valen fue uno de los comandantes de los Centinelas de la Noche que lideró innumerables batallas contra las fuerzas del Infierno. 
                                    Sin embargo, después de que su hijo pereciera en batalla, 
                                    se volvió loco e fue implacablemente perseguido por visiones demoníacas de su hijo viviendo en una existencia torturada. 
                                    Valen pronto cayó en la desesperación de devolverle la vida a su hijo a costa de traicionar a su gente.</p>
                                
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
                
                    <!-- popup añadido 2 -->
		<div id="gal8" class="pop-overlay animate">
			<div class="popup">
                             <?= Html::img('@web/images/DE_Samur_Maykr.png', ['alt'=>'imagen dentro POPUP 4.1'], ['class'=>'img-fluid']);?>
			
				<p class="mt-4">The Tyrant es una recreación del Cyberdemon original de Doom en lugar de la variante más voluminosa y 
                                    pesada que se encontró en el juego anterior. 
                                    Es de color tostado, con largos cuernos negros curvados y dos pequeños ojos negros y brillantes,
                                    muy separados entre sí. El tirano tiene una cara plana con fosas nasales al ras y carece de cualquier tipo de labios, 
                                    exponiendo dientes afilados con dos grandes colmillos hacia arriba como los caninos inferiores. 
                                    Carece de la mandíbula dividida que se encuentra en la versión Doom 2016 del monstruo.</p>
                                
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
                
                 <!-- popup añadido 3 -->
		<div id="gal9" class="pop-overlay animate">
			<div class="popup">
                             <?= Html::img('@web/images/samel hayden.jpg', ['alt'=>'imagen dentro POPUP 4.1'], ['class'=>'img-fluid']);?>
			
				<p class="mt-4">El Dr. Samuel Hayden es el ejecutivo de la Union Aerospace Corporation (UAC) que 
                                    supervisó los proyectos de investigación de la energía Argent en la base de Marte. 
                                    Es un personaje secundario en Doom (2016) y Doom Eternal , que ayuda a guiar al protagonista.</p>
                                
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
                
                          <!-- popup añadido 4 -->
		<div id="gal10" class="pop-overlay animate">
			<div class="popup">
                             <?= Html::img('@web/images/khan maykr.png', ['alt'=>'imagen dentro POPUP 4.1'], ['class'=>'img-fluid']);?>
			
				<p class="mt-4">Khan Maykr (o Hacedor Supremo) es la gobernante máxima del reino de Urdak y la líder de los Maykrs, 
                                    una raza antigua altamente avanzada de seres extraterrestres con aire angelical. 
                                    Ella sirve como la antagonista inicial de Doom Eternal.</p>
                                
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
                
                               <!-- popup añadido 5 -->
		<div id="gal11" class="pop-overlay animate">
			<div class="popup">
                             <?= Html::img('@web/images/apariciones.png', ['alt'=>'imagen dentro POPUP 4.1'], ['class'=>'img-fluid']);?>
			
				<p class="mt-4">Las Apariciones (wraiths), también conocidas como las Apariciones elementales o los Primogénitos, eran seres divinos originarios del reino de Argent D'Nur.

Fueron venerados y adorados por los Argenta, quienes formaron los Centinelas de la Noche para protegerlos. 
Ellos, a su vez, empoderaron a los Centinelas, haciéndolos casi invencibles contra las fuerzas demoníacas del Infierno.</p>
                                
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
                
                         <!-- popup añadido 6 -->
		<div id="gal12" class="pop-overlay animate">
			<div class="popup">
                             <?= Html::img('@web/images/D4_Centinelas.jpg', ['alt'=>'imagen dentro POPUP 4.1'], ['class'=>'img-fluid']);?>
			
				<p class="mt-4">Los Centinelas de la Noche (night sentinels) eran una orden de santos caballeros que protegían la Ciudad de Argent y una vez se enfrentaron contra los ejércitos del Infierno en el reino de Argent D'Nur eones atrás.

Aparecen como caballeros tecnológicamente avanzados con trajes de armadura blanca y munidos principalmente con armas en forma de espada.
Según el señor de la Cuarta Edad del Infierno, el Doom Slayer "llevaba la corona" de los Centinelas de la Noche, 
y por lo tanto puede haber sido considerado como su líder.</p>
                                
				<a class="close" href="#gallery">&times;</a>
			</div>
		</div>
		<!-- //popup -->
                
	</section>
	<!--// Galeria  -->

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\ContactForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use yii\captcha\Captcha;

$this->title = 'Contact';
$this->params['breadcrumbs'][] = $this->title;
?>
<p class="text-center"> <?= Html::img('@web/images/contactanos logo.png', ['alt'=>'Logo doom wiki galeria'], ['class'=>''] );?> </p>
	<!-- contacto -->
	<section class="contact py-5" id="contact">
		<div class="container py-md-5">
			<div class="row">
				<div class="col-lg-4 contact-left">
                                    
					<p>Contacta con nosotros </p>
                                        <h3 style="color: white" class="heading">Ponerse en contacto</h3>
					<p class="mt-3">Si tiene alguna consulta o desea hablar con nosotros, 
                                            contáctenos para obtener más detalles a continuación.
                                            información dada.</p>

				</div>
				<div class="col-lg-8 about-text">
					<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d25471195.108102433!2d-21.676556475283963!3d38.80321169797352!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0xc42e3783261bc8b%3A0xa6ec2c940768a3ec!2zRXNwYcOxYQ!5e0!3m2!1ses!2ses!4v1586875129198!5m2!1ses!2ses" 
						class="map" style="border:0" allowfullscreen=""></iframe>
				</div>
				<div class="col-lg-8 mt-5">
					<form action="#" method="post">
						<div class="row main-w3layouts-sectns">
							<div class="col-md-6 w3-btm-spc form-text1">
								<input type="text" name="Name" placeholder="nombre" required="">
							</div>
							<div class="col-md-6 w3-btm-spc form-text2">
								<input type="text" name="Phone no" placeholder="número de telefono" required="">
							</div>
						</div>
						<div class="row main-w3layouts-sectns">
							<div class="col-md-6 w3-btm-spc form-text1">
								<input type="email" name="email" placeholder="Escribe tu email" required="">
							</div>
							<div class="col-md-6 w3-btm-spc form-text2">
								<input type="text" name="subject" placeholder="anotación" required="">
							</div>
						</div>
						<div class="main-w3layouts-sectns ">
							<div class="w3-btm-spc form-text2 p-0">
								<textarea placeholder="Pon tu comentario aqui"></textarea>
							</div>
						</div>
						<button class="btn">Enviar</button>
					</form>
				</div>
                            
                            
                            <!-- cambiar de lugar a contactanos -->
				<div class="col-lg-4 mt-5">
					<div class="contact-info">
						<div class="footer-style-w3ls">
							<p><span class="fa fa-map-marker" aria-hidden="true"></span><strong> Localización</strong> : ,
								Portal nº2, Piso Nº2A, España.</p>
						</div>
						<div class="footer-style-w3ls mt-2">
							<p><span class="fa fa-phone" aria-hidden="true"></span><strong> Telefono</strong> : +942 67 14 29</p>
						</div>
						<div class="footer-style-w3ls mt-2">
							<p><span class="fa fa-fax" aria-hidden="true"></span><strong> Fax</strong> : +121 098 8807 9987</p>
						</div>
						<div class="footer-style-w3ls mt-2">
							<p><span class="fa fa-envelope" aria-hidden="true"></span><strong> Email</strong> : <a
									href="mailto:info@example.com">DOOMWIKIAPPASSIST@gmail.com</a></p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //contacto -->
<?php

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>

<div class="site-error">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="verificado">
 <?= Html::img('@web/images/commentario mandado.jpg', ['alt'=>'imagen de error web'], ['class'=>'img-somos']);?>
    </div>
    <br>
    <br>
    <p>
       Su mensaje a sido mandado sin problemas, espere a su respuesta.
        
    </p>
    <p>
        Si ve que su respuesta se demora, no dude en mandarnos un correo.
    </p>

</div>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>
<br>

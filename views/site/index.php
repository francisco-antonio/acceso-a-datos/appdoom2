<?php

  use yii\helpers\Html;
/* @var $this yii\web\View */

$this->title = 'DOOMWIKIAPP';
?>

<!DOCTYPE html>
    
<html lang="es">



<body>

	<!-- header -->
	
	<!-- //header -->


	<!-- MAIN SLIDER BORRADO -->
    <div style="background-color:green;">
    
    </div>
	<head>

        <!-- Resumen -->
           <br>
       <br>
       <br>
       <br>
       <br>
   <div class="text-centered div1"> 
    
   
  <p style="color: black"> Esta aplicación web está dedicada a explicar e informar todo lo posible sobre el juego DOOM, el cual tiene una historia interesante, a la que no se le da demasiada importancia. Finalmente, en la parte superior izquierda de la app se encuentra nuestro logo.</p>
   <?= Html::img('@web/images/logo app.png', ['alt'=>'Logotipo Doom Wiki'], ['class'=>''] );?>

        </div>
        <br>
        <br>
    </head>
	<!-- /MAIN SLIDER BORRADO -->

	<!-- SOBRE EL DOOM -->
	<section class="about py-5" id="about">
		<div class="container py-lg-5 py-md-3">
			<div class="row">
				<div class="col-lg-6 about-padding">
                    
					<h4 style="color: white" class="mt-lg-5">¿QUE ES EL DOOM?</h4>
                    
					<p align="justify " style="color: whitesmoke" class="mt-3">Doom (oficialmente escrito DOOM, y ocasionalmente DooM por los fans, basado en el estilo de las letras del logo de Doom)  es un videojuego FPS (First Person Shooter o disparos en primera persona) creado por Id Software en 1993. El Doom original funcionaba bajo el sistema operativo MS-DOS.

El juego consiste en personificar a un marine espacial que se encuentra de rutina en una estación en Phobos , una de las lunas de Marte. En un segundo, las puertas del Infierno quedan abiertas, dejando libres a un sinnúmero de demonios, espíritus inmundos, zombis, que infestan la base en cuestión de horas. Eres el único ser humano superviviente en la estación y tu misión es ir logrando pasar con vida de nivel en nivel (como en el Wolfenstein 3D).</p>
                    
				</div>
                
				<div class="col-lg-6 px-sm-0 img-div">
                                    <?= Html::img('@web/images/ab1.jpg', ['alt'=>''], ['class'=>'img-fluid']);?>
					 <?= Html::img('@web/images/ab1-alt.jpg', ['alt'=>''], ['class'=>'img-fluid position-img1']);?>
					<!--<img src="images/ab1-alt.jpg" alt="" class="img-fluid position-img" /> -->
				</div>
                
				<div class="col-lg-6 px-sm-0 img-div mt-lg-0 mt-md-4 mt-0">
                                      <?= Html::img('@web/images/ab2.jpg', ['alt'=>''], ['class'=>'img-fluid']);?>
					 <?= Html::img('@web/images/ab2-alt.jpg', ['alt'=>''], ['class'=>'img-fluid position-img1']);?>
					 <?= Html::img('@web/images/ab3-alt.jpg', ['alt'=>''], ['class'=>'img-fluid position-img1']);?>
				</div>
                
				<div class="col-lg-6 about-padding">
                    
					<h4 style="color: whitesmoke" class="mt-lg-5">Historia Original</h4>
                    
					<p align="justify " style="color: whitesmoke" class="mt-3">Eres un marine, de los más fuertes y entrenados de la Tierra, experimentado en combate y listo para la acción. Hace 3 años golpeaste a un oficial superior por ordenar a sus soldados que dispararan contra un grupo de manifestantes civiles. Considerándote como peligroso, decidieron trasladarte a la base en Marte, sector espacial de la UAC (Union Aerospace Corporation). La UAC es un conglomerado multi-planetario con instalaciones de investigación y desechos radioactivos en el planeta Marte y sus dos lunas, Phobos y Deimos. Tus primeros días los pasaste sentado en la sala de vigilancia, viendo videos restringidos y material ultrasecreto como si fuera TV por cable, no habiendo mucha acción allí. Ya habiendo pasado algún tiempo, repentinamente Marte recibió un mensaje desde Phobos "¡Requerimos de soporte militar inmediato! ¡Algo terrible y monstruoso está saliendo por los portales! ¡Los sistemas no responden! Los sistemas computarizados se han vuelto locos!" El resto era simplemente incoherente.</p>
                    
				</div>
			</div>
		</div>
	</section>
	<!-- //SOBRE EL DOOM -->

	<!-- servicios -->
	<section class="services py-5 div2" id="services">
		<div class="container py-xl-5 py-lg-3 ">
			<div class="row py-xl-3 ">
				<div class="col-lg-4 mt-lg-5 ">
					<p style="color: black" class="title">Servicios</p>
                                        
                                        <h3 style="color: black" class="heading mb-4 ">Somos buenos en</h3>
                                        <p style="color: black"> *Dar informacion y detalles de la historia.</p> 
                                        
                                        <p style="color: black">*Responder a curiosidades, las cuales mostraran algún dato curioso.</p>
                                      
                                        <p style="color: black" >*Datos de los jugadores que más lejos llegaron en el juego. </p>
				</div>
				<div class="col-lg-8 mt-lg-0 mt-5">
					<div class="row">
						<div class="col-md-6">
							<div class="bottom-gd-ser p-4 ">
								<div class="row ">
									<div class="img-somos">
                                                                            <!-- icono shop cambiar -->
                                                                            
                                                                            <?= Html::img('@web/images/logo galeria.png', ['alt'=>'Logo doom wiki galeria'], ['class'=>'img-somos'] );?>   
									<!--img style="align-items:center" alt="Logotipo Doom Wiki" src="images\logo app.png" width="80" height="70"  /-->	
									</div>
									<div class="col-sm-10 bottom-gd-content mt-sm-0 mt-4  ">
										<h4 align="justify"></h4>
                                                                              
										<p >En ella podrás ver algunos de los personajes y enemgios con un poco de información de cada uno, 
                                                                                    al seleccionar la imagen, esta se ampliará y se mostrará la descripción </p>
									</div>
								</div>
							</div>
						</div>
						<div class="col-md-6 my-md-0 my-4">
							<div class="bottom-gd-ser p-4">
								<div class="row">
									<div class="img-somos">
                                                                            
                                                                            <!-- cambiar barita magica -->
                                                                            
									 <?= Html::img('@web/images/logo app.png', ['alt'=>'Logo doom wiki galeria'], ['class'=>''] );?> 
                                                                                <!-- cambiar barita magica -->
                                                                                
									</div>
									<div class="col-sm-10 bottom-gd-content mt-sm-0 mt-4">
										<h4 class=""></h4>
										<p text-ALIGN="justify">Aquí se explica el funcionamiento de la aplicación web, 
                                                                                 además de ver unos resumenes de DOOM y algo de su historia, 
                                                                                 tambiénse podrá elegir alguna curiosidad para ver algunos datos importantes del ranking del juego.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="row mt-md-5">
						<div class="col-md-6">
							<div class="bottom-gd-ser p-4">
								<div class="row">
									<div class="img-somos">
                                                                            
                                                                            <!-- cambiar copo de nieve -->
                                                                            
										 <?= Html::img('@web/images/contactanos logo.png', ['alt'=>'Logo doom wiki galeria'], ['class'=>''] );?> 
                                                                                
                                                                                 <!-- cambiar copo de nieve -->
                                                                                 
									</div>
									<div class="col-sm-10 bottom-gd-content mt-sm-0 mt-4">
										<h4 class="mb-sm-3 mb-2"> </h4>
										<p> En contáctanos podrás mandar tus comentarios o dudas sobre el juego, 
                                                                                    tanto como de nuestra aplicación web, 
                                                                                    como también si tienes una petición.</p>
									</div>
								</div>
							</div>
                                                    
						</div>
                                            
                                            
						<div class="col-md-6 mt-md-0 mt-4">
							<div class="bottom-gd-ser p-4">
								<div class="row">
									<div class="img-somos">
                                                                            
                                                                            <!-- cambiar icono del ojo -->
                                                                            
										 <?= Html::img('@web/images/logo sesion.png', ['alt'=>'Logo doom wiki galeria'], ['class'=>''] );?> 
                                                                                
                                                                                 <!-- cambiar icono del ojo -->
									</div>
									<div class="col-sm-10 bottom-gd-content mt-sm-0 mt-4">
										<h4 class="mb-sm-3 mb-2"></h4>
										<p>En estos momentos solo pueden iniciar sesión los usuarios administradores, 
                                                                                    ya que aún esta en proceso el registro de sesión y rediseño.</p>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //servicio -->
        
<!-- Botones consultas posibles -->
        <br>
        <br>
        <br>
<!-- Botones consultas posibles -->

	<!-- stats-->
	<section class="stats-info" id="facts">
		<div class="overlay py-5">
			<div class="container py-lg-5">
				<p class="center text-center">Estadisticas</p>
				<h3 class="heading mb-5 text-center"></h3>
				<div class="row">
					<div class="col-lg-3 col-sm-6 stats-grid-w3-agile">
						<div class="row">
							<div class="col-4">
								<div class="icon-right-w3ls text-center">
									<span class="fa fa-female"></span>
								</div>
							</div>
							<div class="col-8">
								<p class="counter text-wh">1500+</p>
								<p class="text-li">Jugadores Online</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 stats-grid-w3-agile mt-sm-0 mt-4">
						<div class="row">
							<div class="col-4">
								<div class="icon-right-w3ls text-center">
									<span class="fa fa-trophy"></span>
								</div>
							</div>
							<div class="col-8">
								<p class="counter text-wh">800+</p>
								<p class="text-li">Visitas</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 stats-grid-w3-agile mt-lg-0 mt-4">
						<div class="row">
							<div class="col-4">
								<div class="icon-right-w3ls text-center">
									<span class="fa fa-weibo"></span>
								</div>
							</div>
							<div class="col-8">
								<p class="counter text-wh">1</p>
								<p class="text-li">Usuarios en la app</p>
							</div>
						</div>
					</div>
					<div class="col-lg-3 col-sm-6 stats-grid-w3-agile mt-lg-0 mt-4">
						<div class="row">
							<div class="col-4">
								<div class="icon-right-w3ls text-center">
									<span class="fa fa-smile-o"></span>
								</div>
							</div>
							<div class="col-8">
								<p class="counter text-wh">6/10</p>
								<p class="text-li">Puntuacion app</p>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- //stats -->
        
        <!-- consultas base de datos -->
   <section>
       <br>
       <div class="logoC">
       <h1 ><?= Html::img('@web/images/Curiosidades.png', ['alt'=>'Logotipo Galeria doom'], ['class'=>''] );?></h1>
       </div>
<br>
         <h2 style="color: white"><i>Aquí puedes ver algunas curiosidades del juego,
          así como algunos de los jugadores que llegaron lejos.</i></h2> <br><br><br>

  <div class="d-flex justify-content-center">
   <div class="row">
       
       
       <!-- consulta 1 -->
       
    <div class="col-sm-4">
        <div class="consultas">
        <h2>¿Cuántos personajes tiene el juego en total?</h2>
        <p> Prersonajes que tiene el juego en total</p>
        <br>
      <?= Html::a('Ver', ['/site/consulta1a'], ['class' => 'botonCon']) ?>
 <br><br>
 
 
        </div>
        
    </div>
       
       <!-- consulta 2 -->
       
    <div class="col-sm-4">
        <div  class="consultas">
        <h2>¿Cuántas razas tienen los demonios?</h2>
        <p>Razas de demonios que tiene el juego</p>
        <br>
      <?= Html::a('Ver', ['/site/consulta2a'], ['class' => 'botonCon']) ?>
     <br><br>
      
      </div>
    </div>
       
       <!-- consulta 3  -->
       
    <div class="col-sm-4">
        <div class="consultas">
        <h2>¿Además de poder llevar cartuchos la escopeta, puede tener algo más?</h2>
        <p>Municiones extra de la escopeta</p>
         <br>
            <?= Html::a('Ver', ['site/consulta3a'], ['class' => 'botonCon']) ?>
                   <br><br>
        </div>
        
    </div>
       
       <!-- consulta 4 -->
       
      <div class="col-sm-4">
        <div class="consultas">
         <h2>¿Qué jugadores tuvieron la mayor puntuación en modo arcade?</h2>
        <p>Jugadores con mayor puntuación</p>
         <br>
            <?= Html::a('Ver', ['site/consulta4a'], ['class' => 'botonCon']) ?>
                    <br><br>
        </div>
    </div>
       
        <!-- consulta 5 -->
              
     <div class="col-sm-4">
        <div class="consultas">
        <h2>¿Cuál es la cantidad de demonios y en que mapas puedo verlos?</h2>
        <p>cantidad y demonios donde aparecen</p>
         <br>
      <?= Html::a('Ver', ['site/consulta5a'], ['class' => 'botonCon']) ?>
            <br><br>
      </div>
    </div>
        
       <!-- consulta 6 -->
       
     <div class="col-sm-4">
        <div class="consultas">
        <h2>¿Cuál es la media del jugador con mayor puntuacion segun sus partidas en España?</h2>
        <p>Nombre del jugador con mejores estadisticas</p>
         <br>
      <?= Html::a('Ver', ['site/consulta6a'], ['class' => 'botonCon']) ?>
             <br><br>
      </div>
    </div>
       
       <!-- consulta 7 -->
       
     <div class="col-sm-4">
        <div class="consultas">
        <h2>¿Cuál es el nombre de los demonios y cual es su raza de cada uno?</h2>
        <p>Nombre de los demonios con la raza a la que pertenecen</p>
         <br>
      <?= Html::a('Ver', ['site/consulta7a'], ['class' => 'botonCon']) ?>
              <br><br>
      </div>
    </div>
       
      <!-- consulta 8 -->
       
      <div class="col-sm-4">
       <div class="consultas">
        <h2>¿Cuantas zonas secretas tiene el juego en los mapas en total?</h2>
        <p>Número de zonas secretas en total que tiene el juego todos los mapas juntos</p>
         <br>
      <?= Html::a('Ver', ['site/consulta8a'], ['class' => 'botonCon']) ?>
             <br><br>
      </div>
      </div>
       

       
       

       
   
   </section>
	<!-- Newsletter -->
	<section class="subscribe-text py-5" id="subscribe">
		<div class="container py-lg-3">
			<div class="row">
				<div class="col-lg-6">
					<h3 class="heading mb-3">Subscribe newsletter</h3>
					<p>By subscribing to our mailing list you will always get latest Fashion news and updates from us. Please do
						subscribe!</p>
				</div>
				<div class="col-lg-5 offset-lg-1">
					<form action="#" method="post">
						<input type="email" name="Email" placeholder="Pon tu email..." required="">
						<button class="btn1"><span class="fa fa-paper-plane" aria-hidden="true"></span></button>
						<div class="clearfix"> </div>
					</form>
					<p class="mt-3">We respect your privacy and will never share your email address with any person or
						organization.</p>
				</div>
			</div>
		</div>
	</section>
	<!-- //Newsletter -->

	<!-- PIE DE PAGINA -->
	<footer class="footerv4-w3ls py-sm-5 py-4" id="footer">
		<div class="footerv4-top">
			<div class="container">

				<!-- footer logo -->
				<div class="footer-logo text-center">
					<a href="index.html"><span class="fa fa-stumbleupon" aria-hidden="true"></span> DOOMWIKIAPP </a>
				</div>
				<!-- //PIE DE PAGINA -->

				<!-- footer nav links -->
				
				<!-- //footer nav links -->

				<!-- move top icon -->
			<!--	<div class="text-center">
					<a href="#home" class="move-top text-center"><span class="fa fa-angle-double-up"
							aria-hidden="true"></span></a>
				</div> -->
				<!-- //move top icon -->

				<!-- copyright -->
                             
				<div class="copy-right text-center">
                                    
					<p>© 2020 DOOMWIKIAPP. Todos los derechos reservados |Designado por
						<a href=""> Francisco Antonio Fernandez Peña.</a>
					</p>
				</div>
				<!-- //copyright -->


			</div>
		</div>
	</footer>
	<!-- //footer -->


	<!-- main slider -->
	<script src="js/jquery-3.3.1.min.js"></script>
	<script src="js/owl.carousel.js"></script>
	<script>
		$(document).ready(function () {
			$('.owl-one').owlCarousel({
				loop: true,
				margin: 0,
				nav: false,
				dots: true,
				responsiveClass: true,
				autoplay: true,
				autoplayTimeout: 5000,
				autoplaySpeed: 1000,
				autoplayHoverPause: false,
				responsive: {
					0: {
						items: 1,
						nav: false
					},
					480: {
						items: 1,
						nav: false
					},
					667: {
						items: 1,
						nav: false
					},
					1000: {
						items: 1,
						nav: false
					}
				}
			})
		})
	</script>
	<!-- /main slider -->

</body>

</html>
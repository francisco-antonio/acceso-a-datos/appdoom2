<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Mapas */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mapas-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'nombre_mapa')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'puntos_control')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'foto')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

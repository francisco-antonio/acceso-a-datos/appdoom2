<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "zonas".
 *
 * @property int $cod_zona
 * @property int|null $es_secreta
 * @property string|null $nombre_mapa
 *
 * @property Mapas $nombreMapa
 */
class Zonas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'zonas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_zona'], 'required'],
            [['cod_zona', 'es_secreta'], 'integer'],
            [['nombre_mapa'], 'string', 'max' => 20],
            [['cod_zona'], 'unique'],
            [['nombre_mapa'], 'exist', 'skipOnError' => true, 'targetClass' => Mapas::className(), 'targetAttribute' => ['nombre_mapa' => 'nombre_mapa']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_zona' => 'Zonas secretas',
            'es_secreta' => 'Es Secreta',
            'nombre_mapa' => 'Nombre Mapa',
        ];
    }

    /**
     * Gets query for [[NombreMapa]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getNombreMapa()
    {
        return $this->hasOne(Mapas::className(), ['nombre_mapa' => 'nombre_mapa']);
    }
}

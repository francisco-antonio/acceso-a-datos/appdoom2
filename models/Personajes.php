<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "personajes".
 *
 * @property int $cod_personaje
 * @property string|null $nombre
 * @property string|null $descripcion
 * @property int|null $es_secundario
 *
 * @property Armas[] $armas
 * @property Demonios[] $demonios
 * @property Jugadores $jugadores
 */
class Personajes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'personajes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_personaje'], 'required'],
            [['cod_personaje', 'es_secundario'], 'integer'],
            [['nombre'], 'string', 'max' => 10],
            [['descripcion'], 'string', 'max' => 110],
            [['cod_personaje'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_personaje' => 'Personajes',
            'nombre' => 'Nombre',
            'descripcion' => 'Descripcion',
            'es_secundario' => 'Es Secundario',
        ];
    }

    /**
     * Gets query for [[Armas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getArmas()
    {
        return $this->hasMany(Armas::className(), ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[Demonios]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getDemonios()
    {
        return $this->hasMany(Demonios::className(), ['cod_personaje' => 'cod_personaje']);
    }

    /**
     * Gets query for [[Jugadores]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJugadores()
    {
        return $this->hasOne(Jugadores::className(), ['cod_jugador' => 'cod_personaje']);
    }
}
